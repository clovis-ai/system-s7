#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <string.h>

#include "utils.h"

int main(int argc, char *argv[]) {
	printf("*** Start ***\n");

	long number = 5;
	size_t sizet;

	for (int i = 1; i < argc; i++) {
		size_t length = strlen(argv[i]);
		int result = write(STDOUT_FILENO, argv[i], length);
		exit_if(result != length, "write");

		result = write(1, " ", 1);
		exit_if(result != 1, "write space");
	}

	perror("test");

	printf("*** End ***\n");
	return EXIT_SUCCESS;
}
