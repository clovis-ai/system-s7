#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <fcntl.h>

#include "utils.h"

#define DONNEES "../test.txt"
#define BUFFER_SIZE 4

int main(int argc, char *argv[]) {
	printf("*** Start ***\n");

	int fd1, fd2;
	int nb_read1, nb_read2;
	char buffer[BUFFER_SIZE];

	// --- DOUBLE OPEN ---

	fd1 = open(DONNEES, O_RDONLY);
	exit_if(fd1 == -1, "Could not open file 1");

	fd2 = open(DONNEES, O_RDONLY);
	exit_if(fd2 == -1, "Could not open file 2");

	printf("\nRead with file 1:\n");
	nb_read1 = read(fd1, buffer, BUFFER_SIZE);
	write(STDOUT_FILENO, buffer, nb_read1);

	printf("\nRead with file 2:\n");
	nb_read2 = read(fd2, buffer, BUFFER_SIZE);
	write(STDOUT_FILENO, buffer, nb_read2);

	close(fd1);
	close(fd2);

	// --- OPEN THEN DUP ---

	printf("\n");

	fd1 = open(DONNEES, O_RDONLY);
	exit_if(fd1 == -1, "Could not open file 1");

	fd2 = dup(fd1);
	exit_if(fd2 == -1, "Could not open file 2");

	printf("\nRead with file 1:\n");
	nb_read1 = read(fd1, buffer, BUFFER_SIZE);
	write(STDOUT_FILENO, buffer, nb_read1);

	printf("\nRead with file 2:\n");
	nb_read2 = read(fd2, buffer, BUFFER_SIZE);
	write(STDOUT_FILENO, buffer, nb_read2);

	close(fd1);
	close(fd2);

	printf("\n*** End ***\n");
	return EXIT_SUCCESS;
}
