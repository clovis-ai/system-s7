#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include "utils.h"

#define BUFFER_SIZE 2048

int main(int argc, char *argv[]) {
	long buffer;

	ssize_t r = read(STDIN_FILENO, &buffer, sizeof(buffer));
	exit_if(r == -1, "Error read");

	printf("%ld \n", buffer);

	return EXIT_SUCCESS;
}
