#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "utils.h"

#define PIPE "send-to-server"

#define BUFFER_SIZE 2048

int main(int argc, char *argv[]) {
	printf("*** Start ***\n");

	int created_pipe = mknod(PIPE, S_IFIFO | S_IRUSR | S_IWUSR | S_IWGRP | S_IWOTH, 0);
	exit_if(created_pipe == -1, "Could not create pipe");

	int pipe = open(PIPE, O_RDONLY);
	exit_if(pipe == -1, "Could not open fifo");

	int nb_read;
	char buffer[BUFFER_SIZE];
	while (1) {
		nb_read = read(pipe, buffer, BUFFER_SIZE);
		exit_if(nb_read == -1, "Could not read fifo");

		int nb_written;
		while ((nb_written = write(STDOUT_FILENO, buffer, nb_read)) != nb_read);
		exit_if(nb_written == -1, "Could not write to stdout");
	}

	close(pipe);

	printf("*** End ***\n");
	return EXIT_SUCCESS;
}
