#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <string.h>

#include "utils.h"

#define BUFFER_SIZE 2048

int main(int argc, char *argv[]) {
	char buffer[BUFFER_SIZE];
	size_t nb_read, nb_written;

	while ((nb_read = read(STDIN_FILENO, buffer, BUFFER_SIZE))) {
		nb_written = write(STDOUT_FILENO, buffer, nb_read);
		exit_if(nb_written != nb_read, "write");
	}
	exit_if(nb_read == -1, "read");

	return EXIT_SUCCESS;
}
