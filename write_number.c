#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <string.h>

#include "utils.h"

#define BUFFER_SIZE 2048

int main(int argc, char *argv[]) {
	long number;
	size_t rc, len;

	rc = read(STDIN_FILENO, &number, sizeof number);
	exit_if(rc == -1, "read number");

	rc = read(STDIN_FILENO, &len, sizeof len);
	exit_if(rc == -1, "read size_t");

	if (rc != sizeof(size_t)) {
		fprintf(stderr, "Warning, could not find enough data in the buffer\n");
		return EXIT_FAILURE;
	}

	printf("My number is %ld, of size %zu\n", number, len);

	return EXIT_SUCCESS;
}
