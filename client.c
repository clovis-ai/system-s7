#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <fcntl.h>

#include "utils.h"

#define PIPE "send-to-server"
#define BUFFER_SIZE 2048

int main(int argc, char *argv[]) {
	printf("*** Start ***\n");

	int file = open(PIPE, O_WRONLY);
	exit_if(file == -1, "Could not open pipe");

	int nb_read;
	char buffer[BUFFER_SIZE];
	while ((nb_read = read(STDIN_FILENO, buffer, BUFFER_SIZE))) {
		exit_if(nb_read == -1, "Could not read stdin");

		int nb_written;
		while ((nb_written = write(file, buffer, nb_read)) != nb_read);
		exit_if(nb_written == -1, "");
	}

	close(file);

	printf("*** End ***\n");
	return EXIT_SUCCESS;
}
